package com.q_rim.app3;

import android.content.Context;
import android.media.MediaPlayer;

public class AudioPlayer {

  private MediaPlayer player;

  // stop
  public void stop() {
    if (this.player != null) {
      this.player.release();
      this.player = null;
    }
  }

  // play
  public void play(Context c) {
    stop();       // to verify that there is only one media player playing at once.
    this.player = MediaPlayer.create(c, R.raw.one_small_step_wav);    // use other method if playing from URL


    this.player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
      public void onCompletion(MediaPlayer mp) {
        stop();
      }
    });
    this.player.start();
  }

  // pause
  public void pause() {
    if (this.player.isPlaying()) {
      this.player.pause();
    } else {
      this.player.start();
    }
  }
}
