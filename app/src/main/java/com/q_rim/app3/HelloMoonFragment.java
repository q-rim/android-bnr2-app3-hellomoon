package com.q_rim.app3;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class HelloMoonFragment extends Fragment {

  private Button playButton, pauseButton, stopButton;
  private AudioPlayer player = new AudioPlayer();

  // preserving the media player state during rotation.
  // this will retain the fragment which in turn preserves the media player in the fragment.
  // retaining fragment will preserve all the member variables.
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRetainInstance(true);              // calling setRetainInstance.  By default, this is set to false.
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {

    View v = inflater.inflate(R.layout.fragment_hello_moon, parent, false);

    playButton = (Button)v.findViewById(R.id.play_Button);
    playButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        player.play(getActivity());
      }
    });

    pauseButton = (Button)v.findViewById(R.id.pause_Button);
    pauseButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        player.pause();
      }
    });

    stopButton = (Button)v.findViewById(R.id.stop_Button);
    stopButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //player.stop();
        player.stop();
      }
    });
    return v;
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    this.player.stop();
  }
}
