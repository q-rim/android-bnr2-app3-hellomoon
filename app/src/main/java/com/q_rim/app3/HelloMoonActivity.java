package com.q_rim.app3;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class HelloMoonActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello_moon);
    }
}
